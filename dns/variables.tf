variable "token" {
	description = "Linode API token"
}

variable "secret" {
	description = "TSIG secret"
}
