terraform {
	required_providers {
		linode = {
			source = "linode/linode"
			version = "1.26.0"
		}
	}

	backend "s3" {
		bucket = "treehouse-state"
		key = "tfstate"
		region = "us-southeast-1"
		endpoint = "us-southeast-1.linodeobjects.com"
		skip_credentials_validation = true
		skip_region_validation = true
	}
}

provider "linode" {
	token = var.token
}

resource "linode_domain" "treehouse_domain" {
	domain = "treehouse.systems"
	soa_email = "admin@treehouse.systems"
	type = "master"
}

// Treehouse cluster: kn-oci-sanjose
// TODO(ariadne): decommission me
resource "linode_domain_record" "kn_oci_sanjose_ingress_v4" {
	domain_id = "${linode_domain.treehouse_domain.id}"
	name = "kn-oci-sanjose.ingress"
	record_type = "A"
	target = "152.67.234.163"
	ttl_sec = 30
}

resource "linode_domain_record" "kn_oci_sanjose_ingress_v6" {
	domain_id = "${linode_domain.treehouse_domain.id}"
	name = "kn-oci-sanjose.ingress"
	record_type = "AAAA"
	target = "2603:c024:c000:100::80"
	ttl_sec = 30
}

// Treehouse cluster: kn-sfo2
resource "linode_domain_record" "kn_sfo2_ingress_v4" {
	domain_id = "${linode_domain.treehouse_domain.id}"
	name = "kn-sfo2.ingress"
	record_type = "A"
	target = "104.250.236.4"
	ttl_sec = 30
}

resource "linode_domain_record" "kn_sfo2_ingress_v6" {
	domain_id = "${linode_domain.treehouse_domain.id}"
	name = "kn-sfo2.ingress"
	record_type = "AAAA"
	target = "2602:fd37:1:0:104:250:236:4"
	ttl_sec = 30
}

// Treehouse services: Gitea
resource "linode_domain_record" "gitea_v4" {
	domain_id = "${linode_domain.treehouse_domain.id}"
	name = "gitea"
	record_type = "A"
	target = "104.250.236.2"
	ttl_sec = 30
}

resource "linode_domain_record" "gitea_v6" {
	domain_id = "${linode_domain.treehouse_domain.id}"
	name = "gitea"
	record_type = "AAAA"
	target = "2602:fd37:1:0:104:250:236:2"
	ttl_sec = 30
}

resource "linode_domain_record" "woodpecker_cname" {
	domain_id = "${linode_domain.treehouse_domain.id}"
	name = "woodpecker"
	record_type = "CNAME"
	target = "gitea.treehouse.systems"
	ttl_sec = 30
}

// Treehouse services: Mastodon (running on kn-linode-dallas)
resource "linode_domain_record" "social_cname" {
	domain_id = "${linode_domain.treehouse_domain.id}"
	name = "social"
	record_type = "CNAME"
	target = "kn-sfo2.ingress.treehouse.systems"
	ttl_sec = 30
}

resource "linode_domain_record" "cache_cname" {
	domain_id = "${linode_domain.treehouse_domain.id}"
	name = "cache"
	record_type = "CNAME"
	target = "treehousesystems.b-cdn.net"
	ttl_sec = 30
}

// Treehouse services: Discord redirector.
// TODO(ariadne): This is really now treehouse.systems/discord, but
// we need to keep this one for a while until it can be fully
// decommissioned.
resource "linode_domain_record" "discord_cname" {
	domain_id = "${linode_domain.treehouse_domain.id}"
	name = "discord"
	record_type = "CNAME"
	target = "kn-oci-sanjose.ingress.treehouse.systems"
	ttl_sec = 30
}

// Treehouse services: Minecraft (OVH, managed by Kenneth)
resource "linode_domain_record" "minecraft_cname" {
	domain_id = "${linode_domain.treehouse_domain.id}"
	name = "survival.minecraft"
	record_type = "CNAME"
	target = "survival.treehouse.fork.run"
	ttl_sec = 30
}

// Apex domain settings.
resource "linode_domain_record" "apex_v4" {
	domain_id = "${linode_domain.treehouse_domain.id}"
	name = ""
	record_type = "A"
	target = "152.67.234.163"
	ttl_sec = 30
}

resource "linode_domain_record" "apex_v6" {
	domain_id = "${linode_domain.treehouse_domain.id}"
	name = ""
	record_type = "AAAA"
	target = "2603:c024:c000:100::80"
	ttl_sec = 30
}

resource "linode_domain_record" "apex_mx" {
	domain_id = "${linode_domain.treehouse_domain.id}"
	name = ""
	record_type = "MX"
	target = "mx1.mailbun.net"
	priority = "5"
	ttl_sec = 30
}

resource "linode_domain_record" "apex_spf" {
	domain_id = "${linode_domain.treehouse_domain.id}"
	name = ""
	record_type = "TXT"
	target = "v=spf1 a mx include:spf.mailbun.net ~all"
	ttl_sec = 30
}

resource "linode_domain_record" "apex_dmarc" {
	domain_id = "${linode_domain.treehouse_domain.id}"
	name = "_dmarc"
	record_type = "TXT"
	target = "v=DMARC1; p=none; fo=1; rua=mailto:admin@treehouse.systems"
	ttl_sec = 30
}

resource "linode_domain_record" "apex_domainkey" {
	domain_id = "${linode_domain.treehouse_domain.id}"
	name = "mailbun._domainkey"
	record_type = "TXT"
	target = "v=DKIM1; k=rsa; s=email; p=MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAlJGZN1aCAUd8CqyQA7Akzkvns+Wq/w70ft2xr0B8jFp0DtW8BtyLLAsErpIp5ZTDgReYGgL7cNcSsNQRn+d6ZaOBGlC/gH1T3KYfbsvavOdnbGx9gofi6x8I5QOOLhp7epK5YkaP/Igg58Zm0ni3jdeMCuX+qkJVqn2WVv8IcRtIA3zJrTYOW3lGCj1ieezl5ref+43mFvcUqidToR9XyHNmi1RowmWAofbZASXkNqZiR1P2Jw3s7q6p0fAEz6bODNOmngIlRAaKjBfDVezsaTeQJwsMg9g58GigVTSb9gMHRZon61yxWcCJtcivPug7xAVlVU+MMkDr7MfvUke5KQIDAQAB"
	ttl_sec = 30
}
